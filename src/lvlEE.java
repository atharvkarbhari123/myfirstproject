
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Compaq
 */
public class lvlEE extends javax.swing.JFrame {

    /**
     * Creates new form lvlEE
     */
    public lvlEE() {
        initComponents();//   735,580
    }

    public void yes() {
        popw.setVisible(true);
        score.setVisible(true);
    }

    public void no() {
        popl.setVisible(true);
        score.setVisible(true);
    }

    public void o() {
        a1.setEnabled(false);
        a2.setEnabled(false);
        a3.setEnabled(false);
        b2.setEnabled(false);
        b1.setEnabled(false);
        b3.setEnabled(false);
        c1.setEnabled(false);
        c2.setEnabled(false);
        c3.setEnabled(false);
        d3.setEnabled(false);
        d2.setEnabled(false);
        d1.setEnabled(false);
        e1.setEnabled(false);
        e2.setEnabled(false);
        e3.setEnabled(false);
        f1.setEnabled(false);
        f2.setEnabled(false);
        f3.setEnabled(false);
        g1.setEnabled(false);
        g2.setEnabled(false);
        g3.setEnabled(false);
        h1.setEnabled(false);
        h2.setEnabled(false);
        h3.setEnabled(false);
        i1.setEnabled(false);
        i2.setEnabled(false);
        i3.setEnabled(false);
        j1.setEnabled(false);
        j2.setEnabled(false);
        j3.setEnabled(false);
    }

    public void lose() {//has clicked on wrong answer
        int l = Integer.parseInt(lose.getText());
        int w = Integer.parseInt(win.getText());
        int t = Integer.parseInt(total.getText());

        if (t < 9) {
            if (l == 0) {
                lose.setText(l + 1 + "");
                total.setText(t + 1 + "");
                vertical.setVisible(true);
                JOptionPane.showMessageDialog(null, "Wrong Answer!!");
            } else if (l == 1) {
                lose.setText(l + 1 + "");
                total.setText(t + 1 + "");
                up.setVisible(true);
                JOptionPane.showMessageDialog(null, "Wrong Answer!!");
            } else if (l == 2) {
                lose.setText(l + 1 + "");
                total.setText(t + 1 + "");
                rope.setVisible(true);
                JOptionPane.showMessageDialog(null, "Wrong Answer!!");
            } else if (l == 3) {
                stool.setVisible(false);
                aliveman.setVisible(false);
                deadman.setVisible(true);
                JOptionPane.showMessageDialog(null, "Wrong Answer!!");
                o();
                no();
            }
        } else if (t == 9 && w <= 6) {
            JOptionPane.showMessageDialog(null, "Wrong Answer!!");
            deadman.setVisible(true);
            stool.setVisible(false);
            aliveman.setVisible(false);
            no();
        } else if (t == 9 && w >= 7) {
            JOptionPane.showMessageDialog(null, "Wrong Answer!!");
            happy.setVisible(true);
            aliveman.setVisible(false);
            yes();
        }
    }

    public void win() {
        int n = Integer.parseInt(score.getText());
        int f = n + 50;
        score.setText("" + f);
        int w = Integer.parseInt(win.getText());
        int t = Integer.parseInt(total.getText());

        if (t < 9) {
            aliveman.setVisible(false);
            happy.setVisible(true);
            win.setText(w + 1 + "");
            total.setText(t + 1 + "");
            JOptionPane.showMessageDialog(null, "Correct Answer!!");
            happy.setVisible(false);
            aliveman.setVisible(true);
        } else if (t == 9 && w >= 6) {
            happy.setVisible(true);
            aliveman.setVisible(false);
            JOptionPane.showMessageDialog(null, "Correct Answer!!");
            yes();
        }

    }

    /**
     * double numberr=Math.random(); double ii=numberr*100; int opo=(int)ii;
     * r.setText(""+opo);
     *
     * int x=Integer.parseInt(r.getText());      * if(x>=0&&x<=25)
    {
     * p1.setVisible(true);
     * p2.setVisible(false);
     * p3.setVisible(false);
     * p4.setVisible(false);
     * }
     * else
     * if(x>=26&&x<=50)
     * {
     * p1.setVisible(false);
     * p2.setVisible(true);
     * p3.setVisible(false);
     * p4.setVisible(false);
     * }
     * else
     * if(x>=51&&x<=75)
     * {
     * p1.setVisible(false);
     * p2.setVisible(false);
     * p3.setVisible(true);
     * p4.setVisible(false);
     * }
     * else
     * if(x>=76&&x<=100) { p1.setVisible(false); p2.setVisible(false);
     * p3.setVisible(false); p4.setVisible(true);
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        up = new javax.swing.JLabel();
        aliveman = new javax.swing.JLabel();
        deadman = new javax.swing.JLabel();
        vertical = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        r = new javax.swing.JLabel();
        p9 = new javax.swing.JPanel();
        h = new javax.swing.JLabel();
        j1 = new javax.swing.JRadioButton();
        j2 = new javax.swing.JRadioButton();
        j3 = new javax.swing.JRadioButton();
        p1 = new javax.swing.JPanel();
        a = new javax.swing.JLabel();
        a1 = new javax.swing.JRadioButton();
        a2 = new javax.swing.JRadioButton();
        a3 = new javax.swing.JRadioButton();
        p2 = new javax.swing.JPanel();
        b = new javax.swing.JLabel();
        b2 = new javax.swing.JRadioButton();
        b1 = new javax.swing.JRadioButton();
        b3 = new javax.swing.JRadioButton();
        happy = new javax.swing.JLabel();
        rope = new javax.swing.JLabel();
        score = new javax.swing.JLabel();
        popl = new javax.swing.JLabel();
        popw = new javax.swing.JLabel();
        p10 = new javax.swing.JPanel();
        j = new javax.swing.JLabel();
        i1 = new javax.swing.JRadioButton();
        i2 = new javax.swing.JRadioButton();
        i3 = new javax.swing.JRadioButton();
        p7 = new javax.swing.JPanel();
        g = new javax.swing.JLabel();
        g2 = new javax.swing.JRadioButton();
        g1 = new javax.swing.JRadioButton();
        g3 = new javax.swing.JRadioButton();
        p4 = new javax.swing.JPanel();
        d11 = new javax.swing.JLabel();
        d3 = new javax.swing.JRadioButton();
        d2 = new javax.swing.JRadioButton();
        d1 = new javax.swing.JRadioButton();
        p6 = new javax.swing.JPanel();
        f2 = new javax.swing.JRadioButton();
        f3 = new javax.swing.JRadioButton();
        f1 = new javax.swing.JRadioButton();
        i = new javax.swing.JLabel();
        p3 = new javax.swing.JPanel();
        c = new javax.swing.JLabel();
        c1 = new javax.swing.JRadioButton();
        c2 = new javax.swing.JRadioButton();
        c3 = new javax.swing.JRadioButton();
        p5 = new javax.swing.JPanel();
        d = new javax.swing.JLabel();
        e1 = new javax.swing.JRadioButton();
        e2 = new javax.swing.JRadioButton();
        e3 = new javax.swing.JRadioButton();
        p8 = new javax.swing.JPanel();
        h3 = new javax.swing.JRadioButton();
        f = new javax.swing.JLabel();
        h2 = new javax.swing.JRadioButton();
        h1 = new javax.swing.JRadioButton();
        horizontal = new javax.swing.JLabel();
        stool = new javax.swing.JLabel();
        df1 = new javax.swing.JLabel();
        df = new javax.swing.JLabel();
        lose = new javax.swing.JLabel();
        win = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(734, 579));
        setModalExclusionType(null);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(null);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 0, 0);

        up.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ds.png"))); // NOI18N
        getContentPane().add(up);
        up.setBounds(440, 50, 270, 20);

        aliveman.setIcon(new javax.swing.ImageIcon(getClass().getResource("/manalive.png"))); // NOI18N
        aliveman.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                alivemanComponentHidden(evt);
            }
        });
        getContentPane().add(aliveman);
        aliveman.setBounds(480, 130, 160, 210);

        deadman.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mandead.png"))); // NOI18N
        deadman.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                deadmanComponentHidden(evt);
            }
        });
        getContentPane().add(deadman);
        deadman.setBounds(480, 130, 180, 240);

        vertical.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vs.png"))); // NOI18N
        getContentPane().add(vertical);
        vertical.setBounds(680, 70, 30, 350);

        jLabel2.setFont(new java.awt.Font("Pokemon RS", 0, 1)); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(420, 20, 0, 0);

        r.setFont(new java.awt.Font("Pokemon RS", 0, 1)); // NOI18N
        r.setText("0");
        getContentPane().add(r);
        r.setBounds(474, 20, 0, 1);

        p9.setOpaque(false);

        h.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        h.setText("Q: Fastest man on earth as of 2018 is:");
        h.setOpaque(true);

        j1.setForeground(new java.awt.Color(255, 0, 0));
        j1.setText("Usain Bolt");
        j1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j1ActionPerformed(evt);
            }
        });

        j2.setForeground(new java.awt.Color(255, 0, 0));
        j2.setText("Yohan Blake");
        j2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j2ActionPerformed(evt);
            }
        });

        j3.setForeground(new java.awt.Color(255, 0, 0));
        j3.setText("Justin Gatlin");
        j3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p9Layout = new javax.swing.GroupLayout(p9);
        p9.setLayout(p9Layout);
        p9Layout.setHorizontalGroup(
            p9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p9Layout.createSequentialGroup()
                .addComponent(h)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(p9Layout.createSequentialGroup()
                .addComponent(j1)
                .addGap(34, 34, 34)
                .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addComponent(j3, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        p9Layout.setVerticalGroup(
            p9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p9Layout.createSequentialGroup()
                .addComponent(h)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(j1)
                    .addComponent(j2)
                    .addComponent(j3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(p9);
        p9.setBounds(10, 460, 360, 50);

        p1.setOpaque(false);

        a.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        a.setText("Q: The tallest mountain in the world is:");
        a.setOpaque(true);

        a1.setForeground(new java.awt.Color(255, 0, 0));
        a1.setText("K2");
        a1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                a1MouseClicked(evt);
            }
        });
        a1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a1ActionPerformed(evt);
            }
        });

        a2.setForeground(new java.awt.Color(255, 0, 0));
        a2.setText("Mount Broad Peak");
        a2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a2ActionPerformed(evt);
            }
        });

        a3.setForeground(new java.awt.Color(255, 0, 0));
        a3.setText("Mount Everest");
        a3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                a3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p1Layout = new javax.swing.GroupLayout(p1);
        p1.setLayout(p1Layout);
        p1Layout.setHorizontalGroup(
            p1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p1Layout.createSequentialGroup()
                .addGroup(p1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(a, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p1Layout.createSequentialGroup()
                        .addComponent(a1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(68, 68, 68)
                        .addComponent(a2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(a3, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 36, Short.MAX_VALUE))
        );
        p1Layout.setVerticalGroup(
            p1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, p1Layout.createSequentialGroup()
                .addComponent(a)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(p1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(a1)
                    .addComponent(a2)
                    .addComponent(a3))
                .addContainerGap())
        );

        getContentPane().add(p1);
        p1.setBounds(10, 10, 420, 50);

        p2.setOpaque(false);

        b.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        b.setText("Q: Total number of elements in the periodic table are:");
        b.setOpaque(true);

        b2.setForeground(new java.awt.Color(255, 0, 0));
        b2.setText("100");
        b2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b2ActionPerformed(evt);
            }
        });

        b1.setForeground(new java.awt.Color(255, 0, 0));
        b1.setText("118");
        b1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b1ActionPerformed(evt);
            }
        });

        b3.setForeground(new java.awt.Color(255, 0, 0));
        b3.setText("63");
        b3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p2Layout = new javax.swing.GroupLayout(p2);
        p2.setLayout(p2Layout);
        p2Layout.setHorizontalGroup(
            p2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p2Layout.createSequentialGroup()
                .addGroup(p2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(b, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p2Layout.createSequentialGroup()
                        .addComponent(b1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(b2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(b3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        p2Layout.setVerticalGroup(
            p2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p2Layout.createSequentialGroup()
                .addComponent(b)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(p2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b2)
                    .addComponent(b1)
                    .addComponent(b3))
                .addContainerGap())
        );

        getContentPane().add(p2);
        p2.setBounds(10, 60, 430, 50);

        happy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/happyman.png"))); // NOI18N
        getContentPane().add(happy);
        happy.setBounds(500, 130, 150, 220);

        rope.setIcon(new javax.swing.ImageIcon(getClass().getResource("/noose.png"))); // NOI18N
        getContentPane().add(rope);
        rope.setBounds(550, 50, 60, 190);

        score.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        score.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        score.setText("0");
        getContentPane().add(score);
        score.setBounds(330, 310, 80, 50);

        popl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lose.png"))); // NOI18N
        popl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                poplMouseClicked(evt);
            }
        });
        getContentPane().add(popl);
        popl.setBounds(250, 150, 240, 290);

        popw.setIcon(new javax.swing.ImageIcon(getClass().getResource("/win.png"))); // NOI18N
        popw.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                popwMouseClicked(evt);
            }
        });
        getContentPane().add(popw);
        popw.setBounds(250, 170, 230, 270);

        p10.setOpaque(false);

        j.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        j.setText("Q: Which is the most viewed sport in the world?");
        j.setOpaque(true);

        i1.setForeground(new java.awt.Color(255, 0, 0));
        i1.setText("Cricket");
        i1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i1ActionPerformed(evt);
            }
        });

        i2.setForeground(new java.awt.Color(255, 0, 0));
        i2.setText("Wrestling");
        i2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i2ActionPerformed(evt);
            }
        });

        i3.setForeground(new java.awt.Color(255, 0, 0));
        i3.setText("Football");
        i3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p10Layout = new javax.swing.GroupLayout(p10);
        p10.setLayout(p10Layout);
        p10Layout.setHorizontalGroup(
            p10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p10Layout.createSequentialGroup()
                .addGroup(p10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(j, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p10Layout.createSequentialGroup()
                        .addComponent(i1)
                        .addGap(50, 50, 50)
                        .addComponent(i2)
                        .addGap(81, 81, 81)
                        .addComponent(i3)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        p10Layout.setVerticalGroup(
            p10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p10Layout.createSequentialGroup()
                .addComponent(j)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(i1)
                    .addComponent(i2)
                    .addComponent(i3))
                .addGap(0, 3, Short.MAX_VALUE))
        );

        getContentPane().add(p10);
        p10.setBounds(10, 410, 420, 50);

        p7.setOpaque(false);

        g.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        g.setText("Q: Which of these countries do NOT play international cricket?");
        g.setOpaque(true);

        g2.setForeground(new java.awt.Color(255, 0, 0));
        g2.setText("U.A.E");
        g2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                g2ActionPerformed(evt);
            }
        });

        g1.setForeground(new java.awt.Color(255, 0, 0));
        g1.setText("China");
        g1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                g1ActionPerformed(evt);
            }
        });

        g3.setForeground(new java.awt.Color(255, 0, 0));
        g3.setText("Nepal");
        g3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                g3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p7Layout = new javax.swing.GroupLayout(p7);
        p7.setLayout(p7Layout);
        p7Layout.setHorizontalGroup(
            p7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p7Layout.createSequentialGroup()
                .addGroup(p7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(g)
                    .addGroup(p7Layout.createSequentialGroup()
                        .addComponent(g1)
                        .addGap(52, 52, 52)
                        .addComponent(g2)
                        .addGap(102, 102, 102)
                        .addComponent(g3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8))
        );
        p7Layout.setVerticalGroup(
            p7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p7Layout.createSequentialGroup()
                .addComponent(g, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(g1)
                    .addComponent(g2)
                    .addComponent(g3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(p7);
        p7.setBounds(10, 310, 510, 50);

        p4.setOpaque(false);

        d11.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        d11.setText("Q: How many bones does an average human body have?");
        d11.setOpaque(true);

        d3.setForeground(new java.awt.Color(255, 0, 0));
        d3.setText("206");
        d3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                d3ActionPerformed(evt);
            }
        });

        d2.setForeground(new java.awt.Color(255, 0, 0));
        d2.setText("207");
        d2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                d2ActionPerformed(evt);
            }
        });

        d1.setForeground(new java.awt.Color(255, 0, 0));
        d1.setText("208");
        d1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                d1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p4Layout = new javax.swing.GroupLayout(p4);
        p4.setLayout(p4Layout);
        p4Layout.setHorizontalGroup(
            p4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p4Layout.createSequentialGroup()
                .addGroup(p4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(d11, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p4Layout.createSequentialGroup()
                        .addComponent(d1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(d2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(74, 74, 74)
                        .addComponent(d3, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        p4Layout.setVerticalGroup(
            p4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p4Layout.createSequentialGroup()
                .addComponent(d11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(p4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(d3)
                    .addComponent(d2)
                    .addComponent(d1))
                .addContainerGap())
        );

        getContentPane().add(p4);
        p4.setBounds(10, 160, 450, 50);

        p6.setOpaque(false);

        f2.setForeground(new java.awt.Color(255, 0, 0));
        f2.setText("Malala Yousufzai");
        f2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f2ActionPerformed(evt);
            }
        });

        f3.setForeground(new java.awt.Color(255, 0, 0));
        f3.setText("Justin Bieber");
        f3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f3ActionPerformed(evt);
            }
        });

        f1.setForeground(new java.awt.Color(255, 0, 0));
        f1.setText("Albert Einstein");
        f1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f1ActionPerformed(evt);
            }
        });

        i.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        i.setText("Q: Who is the youngest person to get a noble prize?");
        i.setOpaque(true);

        javax.swing.GroupLayout p6Layout = new javax.swing.GroupLayout(p6);
        p6.setLayout(p6Layout);
        p6Layout.setHorizontalGroup(
            p6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p6Layout.createSequentialGroup()
                .addGroup(p6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(i, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(p6Layout.createSequentialGroup()
                        .addComponent(f1)
                        .addGap(9, 9, 9)
                        .addComponent(f2, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(f3, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(48, 48, 48))
        );
        p6Layout.setVerticalGroup(
            p6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p6Layout.createSequentialGroup()
                .addGroup(p6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(p6Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(p6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(f1)
                            .addComponent(f2)))
                    .addGroup(p6Layout.createSequentialGroup()
                        .addComponent(i)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(f3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(p6);
        p6.setBounds(10, 260, 450, 50);

        p3.setOpaque(false);

        c.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        c.setText("Q: The first human on moon was:");
        c.setOpaque(true);

        c1.setForeground(new java.awt.Color(255, 0, 0));
        c1.setText("Yuri Gagarin");
        c1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c1ActionPerformed(evt);
            }
        });

        c2.setForeground(new java.awt.Color(255, 0, 0));
        c2.setText("Neil Armstrong");
        c2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c2ActionPerformed(evt);
            }
        });

        c3.setForeground(new java.awt.Color(255, 0, 0));
        c3.setText("Rakesh Sharma");
        c3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                c3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p3Layout = new javax.swing.GroupLayout(p3);
        p3.setLayout(p3Layout);
        p3Layout.setHorizontalGroup(
            p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p3Layout.createSequentialGroup()
                .addGroup(p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(p3Layout.createSequentialGroup()
                        .addComponent(c1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(c2, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(c3, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(c, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 36, Short.MAX_VALUE))
        );
        p3Layout.setVerticalGroup(
            p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p3Layout.createSequentialGroup()
                .addComponent(c)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(c1)
                    .addGroup(p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(c2)
                        .addComponent(c3)))
                .addContainerGap())
        );

        getContentPane().add(p3);
        p3.setBounds(10, 110, 420, 50);

        p5.setOpaque(false);
        p5.setLayout(null);

        d.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        d.setText("Q: Which of these is NOT an african country?");
        d.setOpaque(true);
        p5.add(d);
        d.setBounds(0, 0, 382, 22);

        e1.setForeground(new java.awt.Color(255, 0, 0));
        e1.setText("Senegal");
        e1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                e1ActionPerformed(evt);
            }
        });
        p5.add(e1);
        e1.setBounds(0, 24, 81, 23);

        e2.setForeground(new java.awt.Color(255, 0, 0));
        e2.setText("Egypt");
        e2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                e2ActionPerformed(evt);
            }
        });
        p5.add(e2);
        e2.setBounds(107, 24, 80, 23);

        e3.setForeground(new java.awt.Color(255, 0, 0));
        e3.setText("Qatar");
        e3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                e3ActionPerformed(evt);
            }
        });
        p5.add(e3);
        e3.setBounds(259, 24, 100, 23);

        getContentPane().add(p5);
        p5.setBounds(10, 210, 400, 50);

        p8.setOpaque(false);

        h3.setForeground(new java.awt.Color(255, 0, 0));
        h3.setText("Pratibha Patil");
        h3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                h3ActionPerformed(evt);
            }
        });

        f.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        f.setText("Q: Who was the first female prime minister of independant India?");
        f.setOpaque(true);

        h2.setForeground(new java.awt.Color(255, 0, 0));
        h2.setText("Indira Gandhi");
        h2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                h2ActionPerformed(evt);
            }
        });

        h1.setForeground(new java.awt.Color(255, 0, 0));
        h1.setText("Sonia Gandhi");
        h1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                h1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout p8Layout = new javax.swing.GroupLayout(p8);
        p8.setLayout(p8Layout);
        p8Layout.setHorizontalGroup(
            p8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p8Layout.createSequentialGroup()
                .addComponent(h1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(h2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(h3, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(p8Layout.createSequentialGroup()
                .addComponent(f, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        p8Layout.setVerticalGroup(
            p8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(p8Layout.createSequentialGroup()
                .addComponent(f, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(h1)
                    .addComponent(h2)
                    .addComponent(h3))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        getContentPane().add(p8);
        p8.setBounds(10, 360, 520, 50);

        horizontal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/us.png"))); // NOI18N
        getContentPane().add(horizontal);
        horizontal.setBounds(430, 390, 280, 30);

        stool.setIcon(new javax.swing.ImageIcon(getClass().getResource("/stool.png"))); // NOI18N
        getContentPane().add(stool);
        stool.setBounds(550, 310, 60, 100);

        df1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        df1.setText("LEVEL:");
        getContentPane().add(df1);
        df1.setBounds(500, 10, 70, 20);

        df.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        df.setText("EASY");
        getContentPane().add(df);
        df.setBounds(580, 10, 60, 20);

        lose.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lose.setText("0");
        getContentPane().add(lose);
        lose.setBounds(490, 30, 30, 20);

        win.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        win.setText("0");
        getContentPane().add(win);
        win.setBounds(550, 30, 30, 20);

        total.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        total.setText("0");
        getContentPane().add(total);
        total.setBounds(600, 30, 30, 20);

        jLabel3.setFont(new java.awt.Font("Pokemon RS", 0, 1)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/g.jpg"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(0, 0, 740, 570);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void alivemanComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_alivemanComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_alivemanComponentHidden

    private void deadmanComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_deadmanComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_deadmanComponentHidden

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
  /*win.setVisible(false);
  lose.setVisible(false);
  total.setVisible(false);*/
  vertical.setVisible(false);
  up.setVisible(false);
  rope.setVisible(false);
  deadman.setVisible(false);
  happy.setVisible(false);
  popl.setVisible(false);
  popw.setVisible(false);
  score.setVisible(false);

    }//GEN-LAST:event_formWindowOpened

    private void j1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j1ActionPerformed
        win();

        j1.setEnabled(false);
        j2.setEnabled(false);
        j3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_j1ActionPerformed

    private void j2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j2ActionPerformed
        lose();

        j1.setEnabled(false);
        j2.setEnabled(false);
        j3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_j2ActionPerformed

    private void j3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j3ActionPerformed
        lose();

        j1.setEnabled(false);
        j2.setEnabled(false);
        j3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_j3ActionPerformed

    private void a1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_a1MouseClicked

    }//GEN-LAST:event_a1MouseClicked

    private void a1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a1ActionPerformed
        lose();

        a1.setEnabled(false);
        a2.setEnabled(false);
        a3.setEnabled(false);
    }//GEN-LAST:event_a1ActionPerformed

    private void a2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a2ActionPerformed
        lose();

        a1.setEnabled(false);
        a2.setEnabled(false);
        a3.setEnabled(false);
    }//GEN-LAST:event_a2ActionPerformed

    private void a3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_a3ActionPerformed
        win();

        a1.setEnabled(false);
        a2.setEnabled(false);
        a3.setEnabled(false);
    }//GEN-LAST:event_a3ActionPerformed

    private void b2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b2ActionPerformed
        lose();

        b2.setEnabled(false);
        b1.setEnabled(false);
        b3.setEnabled(false);

        // TODO add your handling code here:
    }//GEN-LAST:event_b2ActionPerformed

    private void b1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b1ActionPerformed
        win();

        b2.setEnabled(false);
        b1.setEnabled(false);
        b3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_b1ActionPerformed

    private void b3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b3ActionPerformed
        lose();

        b2.setEnabled(false);
        b1.setEnabled(false);
        b3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_b3ActionPerformed

    private void poplMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_poplMouseClicked
       int n=Integer.parseInt(score.getText());
       String txt=df.getText();
       loseframe ob=new loseframe();
       ob.setVisible(true);
       ob.lev.setText(txt);
       ob.tf.setText(n+"");
       ob.outof.setText("/500");
       this.setVisible(false);   
        // TODO add your handling code here:
    }//GEN-LAST:event_poplMouseClicked

    private void popwMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_popwMouseClicked
       int n=Integer.parseInt(score.getText());
       String txt=df.getText();
       winl ob=new winl();
       ob.setVisible(true);
       ob.lev.setText(txt);
       ob.tf.setText(n+"");
       ob.outof.setText("/500");
       this.setVisible(false);
    }//GEN-LAST:event_popwMouseClicked

    private void i1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i1ActionPerformed
        lose();

        i1.setEnabled(false);
        i2.setEnabled(false);
        i3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_i1ActionPerformed

    private void i2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i2ActionPerformed
        lose();

        i1.setEnabled(false);
        i2.setEnabled(false);
        i3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_i2ActionPerformed

    private void i3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i3ActionPerformed
        win();

        i1.setEnabled(false);
        i2.setEnabled(false);
        i3.setEnabled(false);
    }//GEN-LAST:event_i3ActionPerformed

    private void g2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_g2ActionPerformed
        lose();

        g1.setEnabled(false);
        g3.setEnabled(false);
        g2.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_g2ActionPerformed

    private void g1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_g1ActionPerformed
        win();

        g1.setEnabled(false);
        g3.setEnabled(false);
        g2.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_g1ActionPerformed

    private void g3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_g3ActionPerformed
        lose();

        g1.setEnabled(false);
        g3.setEnabled(false);
        g2.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_g3ActionPerformed

    private void d3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_d3ActionPerformed
        win();

        d3.setEnabled(false);
        d2.setEnabled(false);
        d1.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_d3ActionPerformed

    private void d2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_d2ActionPerformed
        lose();

        d3.setEnabled(false);
        d2.setEnabled(false);
        d1.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_d2ActionPerformed

    private void d1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_d1ActionPerformed
        lose();

        d3.setEnabled(false);
        d2.setEnabled(false);
        d1.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_d1ActionPerformed

    private void f2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f2ActionPerformed
        win();

        f1.setEnabled(false);
        f2.setEnabled(false);
        f3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_f2ActionPerformed

    private void f3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f3ActionPerformed
        lose();

        f1.setEnabled(false);
        f2.setEnabled(false);
        f3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_f3ActionPerformed

    private void f1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f1ActionPerformed
        lose();

        f1.setEnabled(false);
        f2.setEnabled(false);
        f3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_f1ActionPerformed

    private void c1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c1ActionPerformed
        lose();

        c1.setEnabled(false);
        c2.setEnabled(false);
        c3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_c1ActionPerformed

    private void c2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c2ActionPerformed
        win();

        c1.setEnabled(false);
        c2.setEnabled(false);
        c3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_c2ActionPerformed

    private void c3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_c3ActionPerformed
        lose();

        c1.setEnabled(false);
        c2.setEnabled(false);
        c3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_c3ActionPerformed

    private void e1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_e1ActionPerformed
        lose();

        e1.setEnabled(false);
        e2.setEnabled(false);
        e3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_e1ActionPerformed

    private void e2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_e2ActionPerformed
        lose();

        e1.setEnabled(false);
        e2.setEnabled(false);
        e3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_e2ActionPerformed

    private void e3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_e3ActionPerformed
        win();

        e1.setEnabled(false);
        e2.setEnabled(false);
        e3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_e3ActionPerformed

    private void h3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_h3ActionPerformed
        lose();

        h1.setEnabled(false);
        h2.setEnabled(false);
        h3.setEnabled(false);
    }//GEN-LAST:event_h3ActionPerformed

    private void h2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_h2ActionPerformed
        win();

        h1.setEnabled(false);
        h2.setEnabled(false);
        h3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_h2ActionPerformed

    private void h1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_h1ActionPerformed
        lose();

        h1.setEnabled(false);
        h2.setEnabled(false);
        h3.setEnabled(false);// TODO add your handling code here:
    }//GEN-LAST:event_h1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(lvlEE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(lvlEE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(lvlEE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(lvlEE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new lvlEE().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel a;
    private javax.swing.JRadioButton a1;
    private javax.swing.JRadioButton a2;
    private javax.swing.JRadioButton a3;
    private javax.swing.JLabel aliveman;
    private javax.swing.JLabel b;
    private javax.swing.JRadioButton b1;
    private javax.swing.JRadioButton b2;
    private javax.swing.JRadioButton b3;
    private javax.swing.JLabel c;
    private javax.swing.JRadioButton c1;
    private javax.swing.JRadioButton c2;
    private javax.swing.JRadioButton c3;
    private javax.swing.JLabel d;
    private javax.swing.JRadioButton d1;
    private javax.swing.JLabel d11;
    private javax.swing.JRadioButton d2;
    private javax.swing.JRadioButton d3;
    private javax.swing.JLabel deadman;
    private javax.swing.JLabel df;
    private javax.swing.JLabel df1;
    private javax.swing.JRadioButton e1;
    private javax.swing.JRadioButton e2;
    private javax.swing.JRadioButton e3;
    private javax.swing.JLabel f;
    private javax.swing.JRadioButton f1;
    private javax.swing.JRadioButton f2;
    private javax.swing.JRadioButton f3;
    private javax.swing.JLabel g;
    private javax.swing.JRadioButton g1;
    private javax.swing.JRadioButton g2;
    private javax.swing.JRadioButton g3;
    private javax.swing.JLabel h;
    private javax.swing.JRadioButton h1;
    private javax.swing.JRadioButton h2;
    private javax.swing.JRadioButton h3;
    private javax.swing.JLabel happy;
    private javax.swing.JLabel horizontal;
    private javax.swing.JLabel i;
    private javax.swing.JRadioButton i1;
    private javax.swing.JRadioButton i2;
    private javax.swing.JRadioButton i3;
    private javax.swing.JLabel j;
    private javax.swing.JRadioButton j1;
    private javax.swing.JRadioButton j2;
    private javax.swing.JRadioButton j3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lose;
    private javax.swing.JPanel p1;
    private javax.swing.JPanel p10;
    private javax.swing.JPanel p2;
    private javax.swing.JPanel p3;
    private javax.swing.JPanel p4;
    private javax.swing.JPanel p5;
    private javax.swing.JPanel p6;
    private javax.swing.JPanel p7;
    private javax.swing.JPanel p8;
    private javax.swing.JPanel p9;
    private javax.swing.JLabel popl;
    private javax.swing.JLabel popw;
    private javax.swing.JLabel r;
    private javax.swing.JLabel rope;
    private javax.swing.JLabel score;
    private javax.swing.JLabel stool;
    private javax.swing.JLabel total;
    private javax.swing.JLabel up;
    private javax.swing.JLabel vertical;
    private javax.swing.JLabel win;
    // End of variables declaration//GEN-END:variables
}
